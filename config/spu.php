<?php

return [
    'path' => env('SPU_DIR_NAME', 'simple-photo'),
    'converted_path' => env('SPU_CONVERTED_DIR_NAME', 'c'),
    'temp_path' => env('SPU_DIR_TEMP', '/var/tmp'),
    'size' => ['w' => 300, 'h' => 300],
];
