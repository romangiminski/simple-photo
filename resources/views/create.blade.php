@extends('app')

@section('content')
<div class="centerFlex">
    <div class="centered">
        <div class="row">
            <div class="col-md-6">
                <form method="post" action="{{ route('photo.store') }}" enctype="multipart/form-data" id="upload_form">
                    @csrf
                    <div class="form-group">
                        <label for="name">Nazwa</label>
                        <input class="form-control" type="text" name="name" id="name" value={{ old('name') }}>
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    </div>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="image" name="image">
                        <label class="custom-file-label text-left" for="image">Wybierz plik z dysku</label>
                        <span class="text-danger">{{ $errors->first('image') }}</span>
                    </div>
                    <div class="form-group mt-3">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="fit" id="nochange" value="auto" @if(old('fit')=='auto' || empty(old('properties'))) checked @endif>
                            <label class="form-check-label" for="nochange">no change</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="fit" id="contain" value="contain" @if(old('fit')=='contain' ) checked @endif>
                            <label class="form-check-label" for="contain">contain</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="fit" id="cover" value="cover" @if(old('fit')=='cover' ) checked @endif>
                            <label class="form-check-label" for="cover">cover</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="fit" id="scale" value="scale" @if(old('fit')=='scale' ) checked @endif>
                            <label class="form-check-label" for="scale">scale</label>
                        </div>
                    </div>
                    <div class="form-group mt-3">
                        <input class="btn btn-primary" type="submit" value="Wyślij">
                    </div>
                </form>
            </div>
            <div class="col-md-6">
                <div id="image_preview">
                    <p>PREVIEW</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('additional-js')
<script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>
@endpush
@push('scripts')
$(document).ready(function () {
bsCustomFileInput.init()

$('#image').change(function(){
let reader = new FileReader();
reader.onload = (e) => {
$('#image_preview').css('background-image', "url("+ e.target.result + ")");
}
reader.readAsDataURL(this.files[0]);
});

$("input[type='radio']").change(function(){
if (this.value == 'scale') {
$('#image_preview').css('background-size', "300px 300px");
} else {
$('#image_preview').css('background-size', this.value).css('background-position', 'center');
}
});
})
@endpush