@extends('app')

@section('content')
    <div class="row">
        <a href="{{ route('photo.create') }}" class="btn btn-primary">Dodaj nowy</a>
    </div>
    <div class="row">
        @foreach ($photos as $photo)
        <a href="{{ route('photo.show', $photo->id) }}" title="{{ $photo->name }}">
            <div class="image-list-item" style="background-image: url({{ $photo->url }}?ver={{ time() }});"></div>
        </a>
        @endforeach
    </div>
@endsection