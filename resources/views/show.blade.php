@extends('app')

@section('content')
<div class="centerFlex">
    <div class="centered">
        <h4>{{ $photo->name }}</h4>
        <img src="{{ $photo->url }}?ver={{ time() }}" />
        <br /><br />
        <a href="{{ route('photo.create') }}" class="btn btn-primary">Dodaj</a>
        <a href="{{ route('photo.edit', $photo->id) }}" class="btn btn-primary">Edytuj</a>
        <a href="{{ route('photo.index') }}" class="btn btn-primary">Lista</a>
        <a href="{{ route('photo.destroy', $photo->id) }}" class="btn btn-danger" id="photo_delete">Usuń</a>
    </div>
</div>
@endsection

@push('scripts')
$(document).ready( function () {
    $('#photo_delete').click(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var delete_url = $(this).attr("href");
        if (confirm('Na pewno usunąć?!')) {
            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: delete_url,
                data: {_method: "DELETE"},
                success: function() {
                    if (confirm('Obraz został usunięty!')) {
                        window.location.replace("{{ route('photo.index') }}");
                    }
                },
                error: function(a,b,c) {
                    alert(c);
                }
            });
        }
    })
});
@endpush