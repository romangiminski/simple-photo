<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('APP_NAME', 'Simple Photo') }}</title>
        <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}" />
        @stack('styles')
    </head>
    <body>
        <div class="container">
            @yield('content')
        </div>
    </body>
    <script src="{{ asset('js/app.js') }}"></script>
    @stack('additional-js')
    <script>
        @stack('scripts')
    </script>
</html>
