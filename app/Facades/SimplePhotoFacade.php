<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class SimplePhotoFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'SimplePhotoTools';
    }
}
