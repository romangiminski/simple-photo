<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SimplePhotoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                    return [];
                }
            case 'PUT':
            case 'PATCH': {
                    return [
                        'image' => 'mimes:png,jpg,gif,jpeg|max:2048',
                        'name' => 'required|max:512|unique:simple_photos,name,' . request()->route()->simplePhoto->id,
                    ];
                }
            case 'POST': {
                    return [
                        'image' => 'required|mimes:png,jpg,gif,jpeg|max:2048',
                        'name' => 'required|max:512|unique:simple_photos,name',
                    ];
                }
        }
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'image.required' => 'Musisz wskazać plik z dysku',
            'image.mimes' => 'Tylko pliki graficzne PNG, JPG lub GIF',
            'image.max' => 'Za duży plik - maks. 2MB',
            'name.required' => 'Musisz podać tytuł',
            'name.max' => 'Dopuszcalne 512 znaków',
            'name.unique' => 'Plik o takim tytule już istnieje',
        ];
    }
}
