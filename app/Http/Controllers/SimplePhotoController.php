<?php

namespace App\Http\Controllers;

use App\Models\SimplePhoto;
use App\Http\Requests\SimplePhotoRequest;

class SimplePhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $photos = SimplePhoto::all();
        return \view('index', ['photos' => $photos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return \view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\SimplePhotoRequest  $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function store(SimplePhotoRequest $request)
    {
        $newPhotoId = \SPT::storePhoto($request);

        return \redirect(route('photo.show', $newPhotoId));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SimplePhoto  $simplePhoto
     * 
     * @return \Illuminate\Http\Response
     */
    public function show(SimplePhoto $simplePhoto)
    {
        return \view('show', ['photo' => $simplePhoto]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SimplePhoto  $simplePhoto
     * 
     * @return \Illuminate\Http\Response
     */
    public function edit(SimplePhoto $simplePhoto)
    {
        return \view('edit', ['photo' => $simplePhoto]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\SimplePhotoRequest  $request
     * @param  \App\Models\SimplePhoto  $simplePhoto
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(SimplePhotoRequest $request, SimplePhoto $simplePhoto)
    {
        $simplePhotoId = \SPT::storePhoto($request, $simplePhoto);

        return \redirect(route('photo.show', $simplePhotoId));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SimplePhoto  $simplePhoto
     * 
     * @return \Illuminate\Http\Response
     */
    public function destroy(SimplePhoto $simplePhoto)
    {
        $simplePhoto->delete();
        return response()->json('OK', 200);
    }
}
