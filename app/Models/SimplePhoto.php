<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SimplePhoto extends Model
{
    use SoftDeletes;

    protected $casts = [
        'properties' => 'object'
    ];

    protected $fillable = [
        'name',
        'filename',
        'properties',
        'status',
    ];

    public function getUrlAttribute()
    {
        return \Storage::disk('media')->url(config('spu.path') . \DIRECTORY_SEPARATOR . $this->id . \DIRECTORY_SEPARATOR . config('spu.converted_path') . \DIRECTORY_SEPARATOR . $this->filename);
    }

    public function getUrlOriginalAttribute()
    {
        return \Storage::disk('media')->url(config('spu.path') . \DIRECTORY_SEPARATOR . $this->id . \DIRECTORY_SEPARATOR . $this->filename);
    }
}
