<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Library\SimplePhotoTools;

class SimplePhotoServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('SimplePhotoTools', function () {
            return new SimplePhotoTools;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
