<?php

namespace App\Library;

use Spatie\Image\Image;
use Spatie\Image\Manipulations;
use App\Models\SimplePhoto;

class SimplePhotoTools
{
    private $fits = [
        'auto' => Manipulations::FIT_CROP,
        'contain' => Manipulations::FIT_CONTAIN,
        'cover' => Manipulations::FIT_MAX,
        'scale' => Manipulations::FIT_STRETCH,
    ];
    private $width;
    private $height;
    private $path;
    private $converted_path;

    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        $this->width = config('spu.size.w');
        $this->height = config('spu.size.h');
        $this->path = config('spu.path') . \DIRECTORY_SEPARATOR;
        $this->converted_path = config('spu.converted_path') . \DIRECTORY_SEPARATOR;
    }

    /**
     * storePhoto
     *
     * @param  \App\Http\Requests\SimplePhotoRequest  $request
     * @param  \App\Models\SimplePhoto  $simplePhoto
     *
     * @return integer
     */
    public function storePhoto($request, SimplePhoto $simplePhoto = null)
    {
        $attributes = [
            'name' => $request->name,
            'properties' => ['fit' => $request->fit],
            'filename' => $request->image && $request->image->isValid() ? $this->setFilename($request->image) : $simplePhoto->filename,
        ];
        switch ($request->method()) {
            case 'PUT':
            case 'PATCH': {
                    $simplePhoto->update($attributes);
                    if ($request->image) {
                        $request->image->storeAs($this->path . $simplePhoto->id, $simplePhoto->filename, 'media');
                    }
                    break;
                }
            case 'POST': {
                    $simplePhoto = SimplePhoto::create($attributes);
                    $request->image->storeAs($this->path . $simplePhoto->id, $simplePhoto->filename, 'media');
                    break;
                }
        }
        $this->convertImage($simplePhoto);

        return $simplePhoto->id;
    }

    /**
     * Return new filename.
     *
     * @param  UploadedFile  $image
     * @return string
     */
    private function setFilename($image)
    {
        return \Str::slug(\basename($image->getClientOriginalName(), $image->getClientOriginalExtension())) . '.' . \strtolower($image->getClientOriginalExtension());
    }

    /**
     * convertImage
     *
     * @param  \App\Models\SimplePhoto  $simplePhoto
     *
     * @return void
     */
    private function convertImage(SimplePhoto $simplePhoto)
    {
        $photoFilePath = \Storage::disk('media')->path($this->path . $simplePhoto->id . \DIRECTORY_SEPARATOR . $simplePhoto->filename);
        $convertedPath = $this->path . $simplePhoto->id . \DIRECTORY_SEPARATOR . $this->converted_path;
        \Storage::disk('media')->makeDirectory($convertedPath);
        $convertedPhotoPath = \Storage::disk('media')->path($convertedPath . $simplePhoto->filename);

        switch ($simplePhoto->properties->fit) {
            case 'scale':
            case 'contain': {
                    Image::load($photoFilePath)->fit($this->fits[$simplePhoto->properties->fit], $this->width, $this->height)
                        ->save($convertedPhotoPath);
                    break;
                }
            case 'cover': {
                    Image::load($photoFilePath)->crop(Manipulations::CROP_CENTER, $this->width, $this->height)
                        ->save($convertedPhotoPath);
                    break;
                }
            default: {
                    Image::load($photoFilePath)->manualCrop($this->width, $this->height, 0, 0)
                        ->save($convertedPhotoPath);
                }
        }
    }
}
